/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.Locale;

import weather.app.App;
import weather.app.mvp.model.IDbModel;
import weather.app.mvp.model.data.CityIds;
import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.model.data.list.ListWeatherData;
import weather.app.server.RequestHelper;

/**
 * Created by trimf on 07/07/07.
 */
public class PrefsHelper implements IDbModel {
    private static final String EXTRA_PREFIX = "prefs_";
    private static final String PREF_SETTINGS_FILE_NAME = EXTRA_PREFIX + "prefs.xml";
    private static final String PREFS_LIST_WEATHER = EXTRA_PREFIX + ".list_weather";
    private static final String PREFS_CITIES_IDS = EXTRA_PREFIX + ".cities_ids";
    private static final String PREFS_DAILY = EXTRA_PREFIX + ".daily_%d_%d";

    private static volatile PrefsHelper instance;

    public static PrefsHelper getInstance() {
        PrefsHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (RequestHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PrefsHelper();
                }
            }
        }
        return localInstance;
    }

    @Override
    public void saveListWeather(ListWeatherData listWeatherData) {
        SharedPreferences settings = App.getContext().getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_LIST_WEATHER, new Gson().toJson(listWeatherData));
        editor.commit();
    }

    @Override
    public @Nullable ListWeatherData loadListWeather() {
        SharedPreferences settings = App.getContext().getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        if (settings != null) {
            String jsonString = settings.getString(PREFS_LIST_WEATHER, null);
            if (!TextUtils.isEmpty(jsonString)) {
                return new Gson().fromJson(jsonString, ListWeatherData.class);
            }
        }
        return null;
    }

    @Override
    public @NonNull int[] loadCitiesIds() {
        SharedPreferences settings = App.getContext().getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        int[] ids = null;
        if (settings != null) {
            String jsonString = settings.getString(PREFS_CITIES_IDS, null);
            if (!TextUtils.isEmpty(jsonString)) {
                CityIds cityIds = new Gson().fromJson(jsonString, CityIds.class);
                if (cityIds != null) {
                    ids = cityIds.getIds();
                }
            }
        }
        return ids != null ? ids : new int[]{524901, 498817};
    }

    @Override
    public void saveCitiesIds(int[] ids) {
        SharedPreferences settings = App.getContext().getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        CityIds cityIds = new CityIds(ids);
        editor.putString(PREFS_CITIES_IDS, new Gson().toJson(cityIds));
        editor.commit();
    }

    @Nullable
    @Override
    public DailyData loadDaily(int id, int cnt) {
        SharedPreferences settings = App.getContext().getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        if (settings != null) {
            String jsonString = settings.getString(getDailyStringKey(id, cnt), null);
            if (!TextUtils.isEmpty(jsonString)) {
                return new Gson().fromJson(jsonString, DailyData.class);
            }
        }
        return null;
    }

    @Override
    public void saveDaily(DailyData dailyData, int id, int cnt) {
        SharedPreferences settings = App.getContext().getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(getDailyStringKey(id, cnt), new Gson().toJson(dailyData));
        editor.commit();
    }

    private String getDailyStringKey(int id, int cnt){
        return String.format(Locale.US, PREFS_DAILY, id, cnt);
    }

}
