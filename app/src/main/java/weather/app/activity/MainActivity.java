/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.Serializable;

import weather.app.R;
import weather.app.fragment.BaseFragment;
import weather.app.fragment.city.CityFragment;
import weather.app.fragment.list.ListFragment;
import weather.app.mvp.model.data.list.CityListData;
import weather.app.utils.BroadcastHelper;

public class MainActivity extends AppCompatActivity {

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                switch (intent.getAction()) {
                    case BroadcastHelper.ACTION_ERROR:
                        String message = intent.getStringExtra(BroadcastHelper.EXTRA_MESSAGE);
                        showError(message);
                        break;
                    case BroadcastHelper.ACTION_OPEN_CITY:
                        Serializable serializable = intent.getSerializableExtra(BroadcastHelper.EXTRA_CITY_LIST_DATA);
                        if (serializable != null && serializable instanceof CityListData) {
                            addFragment(CityFragment.getInstance((CityListData) serializable));
                        }
                }
            }
        }
    };

    View mFragmentContainer;
    ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFragmentContainer = findViewById(R.id.fragment_container);
        mActionBar = getSupportActionBar();
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                FragmentManager fm = getSupportFragmentManager();
                boolean showBack = fm.getBackStackEntryCount() > 1;
                if (mActionBar != null) {
                    mActionBar.setDisplayHomeAsUpEnabled(showBack);
                    mActionBar.setDisplayShowHomeEnabled(showBack);
                }
            }
        });

        addFragment(new ListFragment());
    }

    public void addFragment(BaseFragment fragment) {
        addFragment(fragment, true);
    }

    public void addFragment(BaseFragment fragment, boolean addToBackStack) {
        hideKeyBoard();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }

    public void hideKeyBoard() {
        try {
            // Check if no view has focus:
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onBackPressed(false);
    }

    public void onBackPressed(boolean fromMenu) {
        if (getCurrentFragment() != null) {
            Fragment currentFragment = getCurrentFragment();
            if (currentFragment != null && currentFragment instanceof BaseFragment) {
                BaseFragment baseFragment = (BaseFragment) currentFragment;
                if (baseFragment.backPressed()) {
                    return;
                }
            }
        }
        FragmentManager fm = getSupportFragmentManager();
        int backStackEntryCount = fm.getBackStackEntryCount();

        if (backStackEntryCount > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    public void showError(String message) {
        if (!TextUtils.isEmpty(message)) {
            Snackbar.make(mFragmentContainer, message, Snackbar.LENGTH_SHORT)
                    .setAction(message, null).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        BroadcastHelper.registerReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BroadcastHelper.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed(true);
        return true;
    }
}
