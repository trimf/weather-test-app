/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.presenter.list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.app.App;
import weather.app.R;
import weather.app.db.PrefsHelper;
import weather.app.mvp.model.ICallback;
import weather.app.mvp.model.data.list.CityListData;
import weather.app.mvp.model.data.list.ListWeatherData;
import weather.app.mvp.presenter.BasePresenter;
import weather.app.mvp.view.IListView;
import weather.app.server.RequestHelper;
import weather.app.utils.BroadcastHelper;

/**
 * Created by trimf on 06/07/2017.
 */

public class ListPresenter extends BasePresenter<IListView> implements IListPresenter {
    ListWeatherData mData;

    @Override
    public void bindView(@NonNull IListView view) {
        super.bindView(view);
        loadDataFromDb();
        loadDataFromServer();
    }

    @Override
    public void unbindView() {
        super.unbindView();
    }

    @Override
    public void addCityClicked() {
        if (setupDone()) {
            view().showAddCityDialog();
        }
    }

    @Override
    public void cityClicked(CityListData cityListData) {
        BroadcastHelper.openCity(cityListData);
    }

    @Override
    public void addCity(int id) {
        int[] ids = PrefsHelper.getInstance().loadCitiesIds();
        int[] newIds = new int[ids.length + 1];
        int i = 0;
        for (int newId : ids) {
            newIds[i] = newId;
            i++;
        }
        newIds[i] = id;
        PrefsHelper.getInstance().saveCitiesIds(newIds);
        loadDataFromServer();
    }

    private void loadDataFromDb() {
        mData = PrefsHelper.getInstance().loadListWeather();
        if (mData != null && setupDone()) {
            if (mData.list != null) {
                view().updateList(mData.list);
            }
        }
    }

    private void loadDataFromServer() {
        RequestHelper.getInstance().getListWeather(new ICallback<ListWeatherData>() {
            @Override
            public void onSuccess(@NonNull ListWeatherData data) {
                dataLoaded(data);
            }

            @Override
            public void onFailure(@Nullable Throwable t) {
                BroadcastHelper.sendError(App.getContext().getString(R.string.error_loading));
            }
        }, PrefsHelper.getInstance().loadCitiesIds());
    }

    private void dataLoaded(@NonNull ListWeatherData data) {
        if (isDataNew(data)) {
            mData = data;
            PrefsHelper.getInstance().saveListWeather(mData);
            if (mData.list != null && setupDone()) {
                view().updateList(mData.list);
            }
        }
    }

    private boolean isDataNew(@NonNull ListWeatherData data) {
        return !data.equals(mData);
    }
}
