/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.presenter.list;

import weather.app.mvp.model.data.list.CityListData;
import weather.app.mvp.presenter.IBasePresenter;
import weather.app.mvp.view.IListView;

/**
 * Created by trimf on 07/07/2017.
 */

public interface IListPresenter extends IBasePresenter<IListView> {
    void addCityClicked();
    void cityClicked(CityListData cityListData);
    void addCity(int id);
}
