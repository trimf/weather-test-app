/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.presenter.city;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.app.App;
import weather.app.R;
import weather.app.db.PrefsHelper;
import weather.app.mvp.model.ICallback;
import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.presenter.BasePresenter;
import weather.app.mvp.view.ICityView;
import weather.app.server.RequestHelper;
import weather.app.utils.BroadcastHelper;

/**
 * Created by trimf on 06/07/2017.
 */

public class CityPresenter extends BasePresenter<ICityView> implements ICityPresenter {
    int mCnt;

    @Override
    public void bindView(@NonNull ICityView view) {
        super.bindView(view);
    }

    @Override
    public void unbindView() {
        super.unbindView();
    }


    @Override
    public void daysSelected(int id, int cnt) {
        mCnt = cnt;
        loadDataFromDb(id, cnt);
        loadDataFromServer(id, cnt);

    }

    private void loadDataFromDb(int id, int cnt) {
        DailyData data = PrefsHelper.getInstance().loadDaily(id, cnt);
        if (data != null && setupDone()) {
            view().updateData(data);
        }
    }

    private void loadDataFromServer(final int id, final int cnt) {
        RequestHelper.getInstance().getDaily(new ICallback<DailyData>() {
            @Override
            public void onSuccess(@NonNull DailyData data) {
                dataLoaded(data, id, cnt);
            }

            @Override
            public void onFailure(@Nullable Throwable t) {
                BroadcastHelper.sendError(App.getContext().getString(R.string.error_loading));
            }
        }, id, cnt);
    }

    private void dataLoaded(@NonNull DailyData data, int id, int cnt) {
        if (isDataNew(data, id, cnt)) {
            PrefsHelper.getInstance().saveDaily(data, id, cnt);
            if (setupDone() && cnt == mCnt) {
                view().updateData(data);
            }
        }
    }

    private boolean isDataNew(@NonNull DailyData data, int id, int cnt) {
        return !data.equals(PrefsHelper.getInstance().loadDaily(id, cnt));
    }
}
