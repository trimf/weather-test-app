/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.presenter.city;

import weather.app.mvp.presenter.IBasePresenter;
import weather.app.mvp.view.ICityView;

/**
 * Created by trimf on 07/07/2017.
 */

public interface ICityPresenter extends IBasePresenter<ICityView> {
    void daysSelected(int id, int cnt);
}
