/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model;

import android.support.annotation.NonNull;

import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.model.data.list.ListWeatherData;

/**
 * Created by trimf on 08/07/2017.
 */

public interface IServerModel {
    void getListWeather(ICallback<ListWeatherData> callback, int[] ids);
    void getDaily(@NonNull final ICallback<DailyData> callback, int id, int cnt);
}
