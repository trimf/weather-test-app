/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data;

/**
 * Created by trimf on 15/07/2017.
 */

public class CityIds{
    private int[] ids;

    public CityIds(int[] ids) {
        this.ids = ids;
    }

    public int[] getIds(){
        return ids;
    }
}
