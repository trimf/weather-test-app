/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data.list;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ListWeatherData {
    public int cnt;
    public List<CityListData> list;

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ListWeatherData)) {
            return false;
        } else {
            Gson gson = new Gson();
            return gson.toJson(this).equals(gson.toJson(obj));
        }
    }
}
