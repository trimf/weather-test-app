/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.model.data.list.ListWeatherData;

/**
 * Created by trimf on 08/07/2017.
 */

public interface IDbModel {
    @Nullable ListWeatherData loadListWeather();

    void saveListWeather(ListWeatherData listWeatherData);

    @NonNull int[] loadCitiesIds();

    void saveCitiesIds(int[] ids);

    @Nullable DailyData loadDaily(int id, int cnt);

    void saveDaily(DailyData dailyData, int id, int cnt);

}
