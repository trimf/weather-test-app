/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data.list;

/**
 * Created by trimf on 15/07/2017.
 */


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TemperatureData implements Serializable{
    public String temp;
    public String pressure;
    public String humidity;
    @SerializedName("temp_min")
    public String tempMin;
    @SerializedName("temp_max")
    public String tempMax;
}
