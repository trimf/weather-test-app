/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data.city;

import com.google.gson.Gson;

import java.util.List;

import weather.app.mvp.model.data.list.ListWeatherData;

/**
 * Created by trimf on 16/07/2017.
 */

public class DailyData {
    public int cnt;
    public List<DayData> list;

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof DailyData)) {
            return false;
        } else {
            Gson gson = new Gson();
            return gson.toJson(this).equals(gson.toJson(obj));
        }
    }
}
