/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data.list;

import java.io.Serializable;

/**
 * Created by trimf on 15/07/2017.
 */

public class CityListData implements Serializable{
    public TemperatureData main;
    public int dt;
    public int id;
    public String name;
}
