/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data.city;

/**
 * Created by trimf on 16/07/2017.
 */

public class TempData {
    public String day;
    public String min;
    public String max;
    public String night;
    public String eve;
    public String morn;
}
