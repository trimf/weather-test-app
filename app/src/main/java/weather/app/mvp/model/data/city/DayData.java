/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.model.data.city;

/**
 * Created by trimf on 16/07/2017.
 */

public class DayData {
    public int dt;
    public TempData temp;
}
