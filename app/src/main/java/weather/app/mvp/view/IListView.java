/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.view;

import java.util.List;

import weather.app.mvp.model.data.list.CityListData;

/**
 * Created by trimf on 06/07/2017.
 */

public interface IListView {
    void updateList(List<CityListData> list);
    void showAddCityDialog();
}
