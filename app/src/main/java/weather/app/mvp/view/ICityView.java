/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.mvp.view;

import weather.app.mvp.model.data.city.DailyData;

/**
 * Created by trimf on 06/07/2017.
 */

public interface ICityView {
    void updateData(DailyData dailyData);
}
