/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by a.baskakov on 22/08/16.
 */
public abstract class BaseFragment extends Fragment {
    /**
     * Возвращает true, если backPressed обработан внутри фрагмента
     *
     * @return
     */
    public abstract boolean backPressed();
}
