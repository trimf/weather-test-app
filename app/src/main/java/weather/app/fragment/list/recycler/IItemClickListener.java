/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.list.recycler;

import weather.app.mvp.model.data.list.CityListData;

/**
 * Created by trimf on 15/07/2017.
 */

public interface IItemClickListener {
    void onClick(CityListData data);
}
