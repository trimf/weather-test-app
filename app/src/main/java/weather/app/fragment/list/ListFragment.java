/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.list;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import weather.app.R;
import weather.app.db.PrefsHelper;
import weather.app.fragment.BaseFragment;
import weather.app.fragment.list.recycler.IItemClickListener;
import weather.app.fragment.list.recycler.ListRecyclerAdapter;
import weather.app.mvp.model.data.list.CityListData;
import weather.app.mvp.presenter.list.ListPresenter;
import weather.app.mvp.view.IListView;

/**
 * Created by trimf on 15/07/2017.
 */

public class ListFragment extends BaseFragment implements IListView {

    private ListPresenter mPresenter;
    private View mRootView;
    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_list, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            View addButton = mRootView.findViewById(R.id.add_button);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPresenter.addCityClicked();
                }
            });

            mPresenter = new ListPresenter();
        }
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.bindView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unbindView();
    }

    @Override
    public boolean backPressed() {
        return false;
    }

    @Override
    public void updateList(List<CityListData> list) {
        ListRecyclerAdapter adapter = new ListRecyclerAdapter(list, getActivity(), new IItemClickListener() {
            @Override
            public void onClick(CityListData data) {
                mPresenter.cityClicked(data);
            }
        });
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showAddCityDialog() {
        //openweather doesn't support any search of id by name,
        // so in test app we will use predefined array even with no localization support
        final SparseArray<String> cities = new SparseArray<>();
        cities.put(2988507, "Paris");
        cities.put(2643741, "City of London");
        cities.put(2950159, "Berlin");
        cities.put(625144, "Minsk");
        cities.put(2673730, "Stockholm");
        cities.put(703448, "Kiev");
        cities.put(4140963, "Washington, D. C.");
        cities.put(3143244, "Oslo");
        cities.put(3067696, "Prague");
        cities.put(745044, "Istanbul");
        cities.put(2761369, "Vienna");
        cities.put(593116, "Vilnius");
        cities.put(5152333, "Dublin");
        cities.put(2993458, "Monaco");
        cities.put(6094817, "Ottawa");
        int[] ids = PrefsHelper.getInstance().loadCitiesIds();
        for (int id : ids) {
            cities.remove(id);
        }
        String[] items = new String[cities.size()];
        for (int i = 0; i < cities.size(); i++) {
            int key = cities.keyAt(i);
            // get the object by the key.
            String city = cities.get(key);
            items[i] = city;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getActivity().getString(R.string.add_city));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                mPresenter.addCity(cities.keyAt(item));
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
