/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.list.recycler;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import weather.app.R;
import weather.app.mvp.model.data.list.CityListData;

/**
 * Created by trimf on 15/07/2017.
 */

public class ListRecyclerAdapter extends RecyclerView.Adapter<ListViewHolder> {
    LayoutInflater mLayoutInflater;

    List<CityListData> mList;
    private IItemClickListener mListener;

    public ListRecyclerAdapter(@NonNull List<CityListData> list, Activity activity, IItemClickListener listener) {
        this.mList = list;
        this.mListener = listener;
        mLayoutInflater = LayoutInflater.from(activity);
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListViewHolder(mLayoutInflater.inflate(R.layout.item_list, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        holder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
