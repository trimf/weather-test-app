/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.list.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import weather.app.R;
import weather.app.mvp.model.data.list.CityListData;

/**
 * Created by trimf on 15/07/2017.
 */

public class ListViewHolder extends RecyclerView.ViewHolder {

    private TextView mName;
    private TextView mTemperature;
    private IItemClickListener mListener;

    public ListViewHolder(View itemView, IItemClickListener listener) {
        super(itemView);
        this.mListener = listener;
        mName = (TextView)itemView.findViewById(R.id.name);
        mTemperature = (TextView)itemView.findViewById(R.id.temperature);

    }

    public void bind(final CityListData data){
        mName.setText(data.name);
        mTemperature.setText(data.main.temp);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClick(data);
            }
        });
    }
}
