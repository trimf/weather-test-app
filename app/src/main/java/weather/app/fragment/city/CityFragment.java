/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.city;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import weather.app.R;
import weather.app.fragment.BaseFragment;
import weather.app.fragment.city.recycler.DayRecyclerAdapter;
import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.model.data.city.DayData;
import weather.app.mvp.model.data.list.CityListData;
import weather.app.mvp.presenter.city.CityPresenter;
import weather.app.mvp.view.ICityView;

/**
 * Created by trimf on 16/07/2017.
 */

public class CityFragment extends BaseFragment implements ICityView {
    public static final String EXTRA_CITY_LIST_DATA = "city_list_data";

    private static final int DAYS_3 = 3;
    private static final int DAYS_7 = 7;

    CityListData mCityListData;


    private CityPresenter mPresenter;
    private View mRootView;
    private RecyclerView mRecyclerView;
    RadioButton mRadioButton3Days;
    RadioButton mRadioButton7Days;

    public static CityFragment getInstance(@NonNull CityListData cityListData) {
        CityFragment fragment = new CityFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CITY_LIST_DATA, cityListData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mPresenter = new CityPresenter();

            Bundle bundle = getArguments();
            if (bundle != null) {
                Serializable serializable = bundle.getSerializable(EXTRA_CITY_LIST_DATA);
                if (serializable != null && serializable instanceof CityListData) {
                    mCityListData = (CityListData) serializable;
                }
            }

            mRootView = inflater.inflate(R.layout.fragment_city, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            TextView name = (TextView) mRootView.findViewById(R.id.name);
            TextView temperature = (TextView) mRootView.findViewById(R.id.temperature);
            TextView pressure = (TextView) mRootView.findViewById(R.id.pressure);
            TextView humidity = (TextView) mRootView.findViewById(R.id.humidity);

            mRadioButton3Days = (RadioButton) mRootView.findViewById(R.id.radio_3_days);
            mRadioButton7Days = (RadioButton) mRootView.findViewById(R.id.radio_7_days);

            CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    if (checked) {
                        switch (compoundButton.getId()) {
                            case R.id.radio_3_days:
                                daysSelected(DAYS_3);
                                break;
                            case R.id.radio_7_days:
                                daysSelected(DAYS_7);
                                break;
                        }
                    }
                }
            };

            mRadioButton3Days.setOnCheckedChangeListener(checkedChangeListener);
            mRadioButton7Days.setOnCheckedChangeListener(checkedChangeListener);

            if (mCityListData != null) {
                name.setText(mCityListData.name);
                temperature.setText(mCityListData.main.temp);
                pressure.setText(mCityListData.main.pressure);
                humidity.setText(mCityListData.main.humidity);
            }
        }

        return mRootView;
    }

    private void daysSelected(int cnt) {
        if (mCityListData != null) {
            mPresenter.daysSelected(mCityListData.id, cnt);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.bindView(this);
        if (!(mRadioButton3Days.isChecked() || mRadioButton7Days.isChecked())) {
            mRadioButton3Days.setChecked(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unbindView();
    }

    @Override
    public boolean backPressed() {
        return false;
    }

    @Override
    public void updateData(DailyData dailyData) {
        List<DayData> list = dailyData.list;
        if (list == null) {
            list = new ArrayList<>();
        }
        DayRecyclerAdapter adapter = new DayRecyclerAdapter(list, getActivity());
        mRecyclerView.setAdapter(adapter);
    }
}
