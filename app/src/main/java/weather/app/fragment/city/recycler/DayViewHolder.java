/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.city.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import weather.app.R;
import weather.app.mvp.model.data.city.DayData;

/**
 * Created by trimf on 15/07/2017.
 */

public class DayViewHolder extends RecyclerView.ViewHolder {

    private TextView mDate;
    private TextView mTemperature;

    public DayViewHolder(View itemView) {
        super(itemView);
        mDate = (TextView) itemView.findViewById(R.id.date);
        mTemperature = (TextView) itemView.findViewById(R.id.temperature);

    }

    public void bind(final DayData data) {
        String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date(data.dt * 1000L));
        mDate.setText(dateStr);
        mTemperature.setText(data.temp.day);
    }
}
