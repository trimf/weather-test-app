/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.fragment.city.recycler;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import weather.app.R;
import weather.app.mvp.model.data.city.DayData;

/**
 * Created by trimf on 15/07/2017.
 */

public class DayRecyclerAdapter extends RecyclerView.Adapter<DayViewHolder> {
    LayoutInflater mLayoutInflater;

    List<DayData> mList;

    public DayRecyclerAdapter(@NonNull List<DayData> list, Activity activity) {
        this.mList = list;
        mLayoutInflater = LayoutInflater.from(activity);
    }

    @Override
    public DayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DayViewHolder(mLayoutInflater.inflate(R.layout.item_day, parent, false));
    }

    @Override
    public void onBindViewHolder(DayViewHolder holder, int position) {
        holder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
