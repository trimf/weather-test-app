/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.server;

import android.support.v4.util.ArrayMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.model.data.list.ListWeatherData;

/**
 * Created by a.baskakov on 16/08/16.
 */
public interface Api {

    @GET("data/2.5/group")
    Call<ListWeatherData> getListWeather(@QueryMap ArrayMap<String, Object> params);

    @GET("data/2.5/forecast/daily")
    Call<DailyData> getDaily(@QueryMap ArrayMap<String, Object> params);
}
