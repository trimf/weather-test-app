/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.server;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import weather.app.mvp.model.ICallback;
import weather.app.mvp.model.IServerModel;
import weather.app.mvp.model.data.city.DailyData;
import weather.app.mvp.model.data.list.ListWeatherData;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class RequestHelper implements IServerModel {

    //todo: We shouldn't keep plain api key, but this is a test app!
    private static final String OPEN_WEATHER_API_KEY = "07e7c2dc3945b2e13b73aabc7c644d1f";

    private static volatile RequestHelper instance;

    public static RequestHelper getInstance() {
        RequestHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (RequestHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new RequestHelper();
                }
            }
        }
        return localInstance;
    }

    @Override
    public void getListWeather(@NonNull final ICallback<ListWeatherData> callback, @NonNull int[] ids) {
        ArrayMap<String, Object> params = new ArrayMap<>();

        StringBuilder idsStringBuilder = new StringBuilder();
        for(int id:ids){
            if(idsStringBuilder.length()>0){
                idsStringBuilder.append(",");
            }
            idsStringBuilder.append(id);
        }
        params.put("id", idsStringBuilder.toString());

        params.put("units", "metric");
        addAppId(params);

        Call<ListWeatherData> call = ApiBuilder.buildRetrofitService().getListWeather(params);

        call.enqueue(new Callback<ListWeatherData>() {
            @Override
            public void onResponse(@NonNull Call<ListWeatherData> call, @NonNull Response<ListWeatherData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListWeatherData> call, @NonNull Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    @Override
    public void getDaily(@NonNull final ICallback<DailyData> callback, int id, int cnt) {
        ArrayMap<String, Object> params = new ArrayMap<>();

        params.put("id", id);
        params.put("cnt", cnt);
        params.put("units", "metric");
        addAppId(params);

        Call<DailyData> call = ApiBuilder.buildRetrofitService().getDaily(params);

        call.enqueue(new Callback<DailyData>() {
            @Override
            public void onResponse(@NonNull Call<DailyData> call, @NonNull Response<DailyData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DailyData> call, @NonNull Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    private void addAppId(@NonNull Map<String, Object> map) {
        map.put("appid", OPEN_WEATHER_API_KEY);
    }

}
