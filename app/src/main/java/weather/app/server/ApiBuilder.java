/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.server;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ApiBuilder {
    private static final String OPEN_WEATHER_URL = "http://api.openweathermap.org/";

    static Api buildRetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiBuilder.OPEN_WEATHER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(Api.class);
    }
}
