/*
 * **************************
 * Copyright (c) 2017.
 * trimf
 * **************************
 */

package weather.app.utils;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import weather.app.App;
import weather.app.mvp.model.data.list.CityListData;

/**
 * Created by trimf on 16/07/2017.
 */

public class BroadcastHelper {
    public static final String ACTION_ERROR = "weather.app.error";
    public static final String ACTION_OPEN_CITY = "weather.app.open_city";

    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_CITY_LIST_DATA = "city_list_data";

    public static void registerReceiver(BroadcastReceiver receiver){
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_ERROR);
        filter.addAction(ACTION_OPEN_CITY);
        LocalBroadcastManager.getInstance(App.getContext()).registerReceiver(receiver, filter);
    }

    public static void unregisterReceiver(BroadcastReceiver receiver){
        LocalBroadcastManager.getInstance(App.getContext()).unregisterReceiver(receiver);
    }

    public static void sendError(String message){
        Intent intent = new Intent(ACTION_ERROR);
        intent.putExtra(EXTRA_MESSAGE, message);
        LocalBroadcastManager.getInstance(App.getContext()).sendBroadcast(intent);
    }

    public static void openCity(CityListData cityListData){
        Intent intent = new Intent(ACTION_OPEN_CITY);
        intent.putExtra(EXTRA_CITY_LIST_DATA, cityListData);
        LocalBroadcastManager.getInstance(App.getContext()).sendBroadcast(intent);
    }
}
